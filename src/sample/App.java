package sample;

import dissimlab.random.SimGenerator;
import dissimlab.simcore.SimControlEvent;
import dissimlab.simcore.SimControlException;
import dissimlab.simcore.SimManager;
import dissimlab.simcore.SimParameters;

import java.util.LinkedList;

public class App implements Runnable {
    int nB = 3;
    int nL = 3;
    int nO = 3;
    int qSize = 10;

    @Override
    public void run() {
        SimManager mgr = SimManager.getInstance();
        SimGenerator generator = new SimGenerator();

        Otoczenie otoczenie = new Otoczenie(100, generator, 2.0, 1.5, nB, nL, nO, qSize);

        try {
            new SimControlEvent(200, SimParameters.SimControlStatus.STOPSIMULATION);
            new PrzybycieAuta(otoczenie, 0);

        } catch (
                SimControlException e) {
            e.printStackTrace();
        }

        try {
            mgr.startSimulation();
        } catch (SimControlException e) {
            e.printStackTrace();
        }
    }

    public App(int nB, int nL, int nO, int qSize){
        this.nB = nB;
        this.nL = nL;
        this.nO = nO;
        this.qSize = qSize;
    }

}
