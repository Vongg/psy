package sample;

public class Auto {
    int id;
    int paliwo = 0;
    boolean czyPaliwo;
    boolean czyMycie;
    double zniecierpliwienie;
    OpuszczenieKolejki opuszczenieKolejki;

    public OpuszczenieKolejki getOpuszczenieKolejki() {
        return opuszczenieKolejki;
    }

    public void setOpuszczenieKolejki(OpuszczenieKolejki opuszczenieKolejki) {
        this.opuszczenieKolejki = opuszczenieKolejki;
    }

    public void setPaliwo(int paliwo) {
        this.paliwo = paliwo;
    }

    public void setCzyPaliwo(boolean czyPaliwo) {
        this.czyPaliwo = czyPaliwo;
    }

    public void setCzyMycie(boolean czyMycie) {
        this.czyMycie = czyMycie;
    }

    public int getPaliwo() {
        return paliwo;
    }

    public boolean isCzyPaliwo() {
        return czyPaliwo;
    }

    public boolean isCzyMycie() {
        return czyMycie;
    }

    public Auto(int id, double zniecierpliwienie, boolean czyMycie, boolean czyPaliwo, int paliwo){
        this.id = id;
        this.zniecierpliwienie = zniecierpliwienie;
        this.czyMycie = czyMycie;
        this.czyPaliwo = czyPaliwo;
        this.paliwo = paliwo;
    }

    public double getZniecierpliwienie() {
        return zniecierpliwienie;
    }


    public int getId() {
        return id;
    }
}
