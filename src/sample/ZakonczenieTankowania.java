package sample;

import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;

import java.text.DecimalFormat;

public class ZakonczenieTankowania extends BasicSimEvent<Dystrybutor, Auto> {
    public ZakonczenieTankowania(Dystrybutor entity, double delay, Auto klient) throws SimControlException {
        super(entity, delay, klient);
    }

    Auto auto;

    @Override
    public Dystrybutor getSimObj() {
        return super.getSimObj();
    }

    Dystrybutor dystrybutor = getSimObj();

    @Override
    protected void stateChange() throws SimControlException {
        dystrybutor.setZajetyDystrybutor(false);

        Auto auto = (Auto)getEventParams();

        new RozpoczecieTankowania(dystrybutor , 0);

        DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setMaximumFractionDigits(2);
        decimalFormat.setMinimumFractionDigits(2);

//        System.out.println("Czas symulacji: [" + decimalFormat.format(simTime()) + "] :: [" + String.format("%-18s",this.getClass()) +
//                "] :: [" + klient.getId() + "] :: dlugosc kolejki: [" + bank.getIloscKlientowWKolejce()
//                + "] :: czy zajete: [" + bank.isZajeteOkienko()+"]");

//        System.out.println(String.format("Czas symulacji: [%05.2f] :: [%-24s] :: id klienta: [%03d] :: dlugosc kolejki: [%02d] :: " +
//                        "czy zajete: [%-5s] :: kolejka zablokowana: [%-5s]",simTime(), this.getClass(), klient.getId(),
//                bank.getIloscKlientowWKolejce(), bank.isZajeteOkienko(), bank.isZablokowana()));
    }

    @Override
    protected void onTermination() throws SimControlException {

    }

    @Override
    protected void onInterruption() throws SimControlException {

    }

    @Override
    public Object getEventParams() {
        return this.transitionParams;
    }
}
