package sample;
import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.BasicSimObj;
import dissimlab.simcore.SimControlException;

import java.text.DecimalFormat;

public class RozpoczecieTankowania extends BasicSimEvent<Dystrybutor, Auto> {
    public RozpoczecieTankowania(Dystrybutor entity, double delay) throws SimControlException {
        super(entity, delay);
    }

    Auto auto;

    @Override
    public Dystrybutor getSimObj() {
        return super.getSimObj();
    }

    Dystrybutor dystrybutor = getSimObj();

    @Override
    protected void stateChange() throws SimControlException {
        auto = (Auto) getEventParams();

        if(dystrybutor.getIloscAutWKolejce() == 0){
            return;
        }else {

            auto = dystrybutor.getKolejka().removeFirst();
            auto.opuszczenieKolejki.interrupt();

            dystrybutor.setIloscAutWKolejceWKolejce(dystrybutor.getIloscAutWKolejce()-1);
            dystrybutor.setZajetyDystrybutor(true);

            double dt = dystrybutor.getGenerator().exponential(1.0/dystrybutor.getMi()) + auto.getPaliwo();
            new ZakonczenieTankowania(dystrybutor, dt, auto);

            DecimalFormat decimalFormat = new DecimalFormat();
            decimalFormat.setMaximumFractionDigits(2);
            decimalFormat.setMinimumFractionDigits(2);

//            System.out.println("Czas symulacji: [" + decimalFormat.format(simTime()) + "] :: [" +  String.format("%-18s",this.getClass()) +
//                    "] :: [" + klient.getId() + "] :: dlugosc kolejki: [" + bank.getIloscKlientowWKolejce()
//                    + "] :: czy zajete: [" + bank.isZajeteOkienko()+"]");

//            System.out.println(String.format("Czas symulacji: [%05.2f] :: [%-24s] :: id klienta: [%03d] :: dlugosc kolejki: [%02d] :: " +
//                            "czy zajete: [%-5s] :: kolejka zablokowana: [%-5s]",simTime(), this.getClass(), auto.getId(),
//                    bank.getIloscKlientowWKolejce(), bank.isZajeteOkienko(), bank.isZablokowana()));

        }

    }

    @Override
    protected void onTermination() throws SimControlException {

    }

    @Override
    protected void onInterruption() throws SimControlException {

    }

    @Override
    public Object getEventParams() {
        return transitionParams;
    }
}
