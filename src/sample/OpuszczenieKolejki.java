package sample;
import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;

import java.sql.SQLOutput;

public class OpuszczenieKolejki extends BasicSimEvent<Dystrybutor, Auto> {

    public OpuszczenieKolejki(Dystrybutor entity, double delay, Auto klient) throws SimControlException {
        super(entity, delay, klient);
    }

    Dystrybutor dystrybutor= getSimObj();
    Auto auto;



    @Override
    protected void stateChange() throws SimControlException {
        if(dystrybutor.getKolejka().contains(auto)) {
            auto = (Auto) getEventParams();
            dystrybutor.getKolejka().remove(auto);

            System.out.println("Opuszczenie kolejki: " + auto.getId());
            System.out.println(String.format("Czas symulacji: [%05.2f] :: [%-24s] :: id klienta: [%03d] :: dlugosc kolejki: [%02d] :: " +
                            "czy zajete: [%-5s]", simTime(), this.getClass(), auto.getId(), dystrybutor.getIloscAutWKolejce(),
                    dystrybutor.isZajetyDystrybutor()));
        }
    }

    @Override
    protected void onTermination() throws SimControlException {

    }

    @Override
    protected void onInterruption() throws SimControlException {

    }

    @Override
    public Object getEventParams() {
        return transitionParams;
    }
}
