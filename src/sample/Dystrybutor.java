package sample;
import dissimlab.broker.INotificationEvent;
import dissimlab.broker.IPublisher;
import dissimlab.random.SimGenerator;
import dissimlab.simcore.BasicSimObj;

import java.util.LinkedList;

public class Dystrybutor extends BasicSimObj {

    private  int limitKolejki = 5;
    private  int iloscAutWKolejce = 0;
    private  LinkedList<Auto> kolejka;
    private  boolean zajetyDystrybutor = false;
    private SimGenerator generator;
    private  double mi;
    private  double at;
    private  double nt;

    public Dystrybutor(int limitKolejki, SimGenerator generator, double mi, double at, double nt) {
        this.limitKolejki = limitKolejki;
        this.kolejka = new LinkedList<Auto>();
        this.generator = generator;
        this.mi = mi;
        this.at = at;
        this.nt = nt;
    }

    public void wstawAuto(Auto auto){
        if(iloscAutWKolejce < limitKolejki) {
            kolejka.addLast(auto);
            iloscAutWKolejce++;

            if(kolejka.getFirst().equals(auto)&& !zajetyDystrybutor){
                try {
                    new RozpoczecieTankowania(this,0);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }else{
            System.out.println("Kolejka pelna lub zablokowana!");
        }
    }

    public void setKolejka(LinkedList<Auto> kolejka) {
        this.kolejka = kolejka;
    }

    public double getAt() {
        return at;
    }

    public double getNt() {
        return nt;
    }

    public Auto getFirstAuto(){
        return kolejka.getFirst();
    }

    public void setZajetyDystrybutor(boolean zajetyDystrybutor) {
        this.zajetyDystrybutor = zajetyDystrybutor;
    }

    public int getLimitKolejki() {
        return limitKolejki;
    }

    public boolean isZajetyDystrybutor() {
        return zajetyDystrybutor;
    }

    public LinkedList<Auto> getKolejka() {
        return kolejka;
    }

    public void setIloscAutWKolejceWKolejce(int iloscAutwWKolejce) {
        this.iloscAutWKolejce = iloscAutwWKolejce;
    }

    public SimGenerator getGenerator() {
        return generator;
    }

    public double getMi() {
        return mi;
    }

    public int getIloscAutWKolejce() {
        return iloscAutWKolejce;
    }

    @Override
    public void reflect(IPublisher iPublisher, INotificationEvent iNotificationEvent) {

    }

    @Override
    public boolean filter(IPublisher iPublisher, INotificationEvent iNotificationEvent) {
        return false;
    }
}
