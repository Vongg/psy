package sample;

import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;
import sample.Otoczenie;

import java.text.DecimalFormat;

public class PrzybycieAuta extends BasicSimEvent<Otoczenie, Object> {

    public PrzybycieAuta(Otoczenie entity, double delay) throws SimControlException {
        super(entity, delay);
    }

    @Override
    public Otoczenie getSimObj() {
        return super.getSimObj();
    }

    Otoczenie otoczenie = getSimObj();

    @Override
    protected void stateChange() {
        double zniecierpliwienie = otoczenie.getGenerator().exponential(1 / otoczenie.getZt());
        boolean czyMycie = otoczenie.getGenerator().nextBoolean();
        boolean czyPaliwo = otoczenie.getGenerator().nextBoolean();
        double pChoose = otoczenie.getGenerator().exponential(1 / otoczenie.getLambda());
        int paliwo = 0;
        int index = 0;
        int tmp = otoczenie.getqSize();

        if (czyPaliwo) {
            if (pChoose <= 1) {
                paliwo = 1;
            } else if (pChoose <= 2) {
                paliwo = 2;
            } else {
                paliwo = 3;
            }
        }

        Auto auto = new Auto(otoczenie.getLicznik(), zniecierpliwienie, czyMycie, czyPaliwo, paliwo);

        if (otoczenie.getLicznik() < otoczenie.getOgraniczenie()) {
            double dt = otoczenie.getGenerator().exponential(1 / otoczenie.getLambda());
            otoczenie.setLicznik(otoczenie.getLicznik() + 1);

            if (auto.isCzyPaliwo()) {
                switch (auto.getPaliwo()) {
                    case 1:      //Benzyna
                        for (int i = 0; i < otoczenie.getdBenzyna().size(); i++) {
                            if (otoczenie.getdBenzyna().get(i).getIloscAutWKolejce() < tmp) {
                                index = i;
                                tmp = otoczenie.getdBenzyna().get(i).getIloscAutWKolejce();
                            }
                        }

                        if (tmp <= otoczenie.getdBenzyna().get(index).getLimitKolejki()) {
                            otoczenie.getdBenzyna().get(index).wstawAuto(auto);

                            try {
                                auto.setOpuszczenieKolejki(new OpuszczenieKolejki(otoczenie.getdBenzyna().get(index),
                                        auto.getZniecierpliwienie(), auto));
                            } catch (Exception e) {

                            }
                            try {
                                new PrzybycieAuta(otoczenie, dt);
                            } catch (Exception e) {

                            }

                        } else {
                            System.out.println("Koniec generowania klientow!");
                        }
                        break;

                    case 2:      //LPG
                        for (int i = 0; i < otoczenie.getdLPG().size(); i++) {
                            if (otoczenie.getdLPG().get(i).getIloscAutWKolejce() < tmp) {
                                index = i;
                                tmp = otoczenie.getdLPG().get(i).getIloscAutWKolejce();
                            }
                        }

                        if (tmp <= otoczenie.getdLPG().get(index).getLimitKolejki()) {
                            otoczenie.getdLPG().get(index).wstawAuto(auto);

                            try {
                                auto.setOpuszczenieKolejki(new OpuszczenieKolejki(otoczenie.getdLPG().get(index),
                                        auto.getZniecierpliwienie(), auto));
                            } catch (Exception e) {

                            }

                            try {
                                new PrzybycieAuta(otoczenie, dt);
                            } catch (Exception e) {

                            }

                        } else {
                            System.out.println("Koniec generowania klientow!");
                        }

                        break;

                    case 3:      //ON
                        for (int i = 0; i < otoczenie.getdON().size(); i++) {
                            if (otoczenie.getdON().get(i).getIloscAutWKolejce() < tmp) {
                                index = i;
                                tmp = otoczenie.getdON().get(i).getIloscAutWKolejce();
                            }
                        }

                        if (tmp <= otoczenie.getdON().get(index).getLimitKolejki()) {
                            otoczenie.getdON().get(index).wstawAuto(auto);

                            try {
                                auto.setOpuszczenieKolejki(new OpuszczenieKolejki(otoczenie.getdON().get(index),
                                        auto.getZniecierpliwienie(), auto));
                            } catch (Exception e) {

                            }

                            try {
                                new PrzybycieAuta(otoczenie, dt);
                            } catch (Exception e) {

                            }
                        } else {
                            System.out.println("Koniec generowania klientow!");
                        }
                        break;
                }
            }

        }


        DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setMaximumFractionDigits(2);
        decimalFormat.setMinimumFractionDigits(2);

//        System.out.println("Czas symulacji: [" +decimalFormat.format(simTime()) + "] :: [" +  String.format("%-18s",this.getClass()) +
//                "] :: [" + klient.getId() + "] :: dlugosc kolejki: [" + otoczenie.getBank().getIloscKlientowWKolejce()
//                + "] :: czy zajete: [" + otoczenie.getBank().isZajetyDystrybutor()+"]");

//        System.out.println(String.format("Czas symulacji: [%05.2f] :: [%-24s] :: id klienta: [%03d] :: dlugosc kolejki: [%02d] :: " +
//                        "czy zajete: [%-5s] :: kolejka zablokowana [%-5s]",simTime(), this.getClass(), auto.getId(), otoczenie.getBank().getIloscKlientowWKolejce(),
//                otoczenie.getBank().isZajeteOkienko(), otoczenie.getBank().isZablokowana()));

    }

    @Override
    protected void onTermination() throws SimControlException {

    }

    @Override
    protected void onInterruption() throws SimControlException {

    }

    @Override
    public Object getEventParams() {
        return null;
    }
}
