package sample;
import dissimlab.broker.INotificationEvent;
import dissimlab.broker.IPublisher;
import dissimlab.random.SimGenerator;
import dissimlab.simcore.BasicSimObj;

import java.util.LinkedList;
import java.util.List;

public class Otoczenie extends BasicSimObj {
    int licznik = 0;
    int qSize = 0;
    int ograniczenie  = 10;
    SimGenerator generator;
    List<Dystrybutor> dBenzyna;
    List<Dystrybutor> dLPG;
    List<Dystrybutor> dON;
    double lambda;
    double zt;

    public int getqSize() {
        return qSize;
    }

    public Otoczenie(int ograniczenie, SimGenerator generator, double lambda, double zt, int numberOfB,
                     int numberOfL, int numberOfO, int qSize) {
        this.ograniczenie = ograniczenie;
        this.generator = generator;
        this.lambda = lambda;
        this.zt = zt;
        this.qSize = qSize;
        this.dBenzyna = dBenzyna;
        this.dLPG = dLPG;
        this.dON = dON;

        dBenzyna = new LinkedList<Dystrybutor>();
        dLPG = new LinkedList<Dystrybutor>();
        dON = new LinkedList<Dystrybutor>();

        for(int i = 0; i<numberOfB; i++){
            dBenzyna.add(new Dystrybutor(qSize, generator, 1.5, 5.0, 5.0));
        }

        for(int i = 0; i<numberOfL; i++){
            dLPG.add(new Dystrybutor(qSize, generator, 1.5, 5.0, 5.0));
        }

        for(int i = 0; i<numberOfO; i++){
            dON.add(new Dystrybutor(qSize, generator, 1.5, 5.0, 5.0));
        }
    }

    public double getZt() {
        return zt;
    }

    public double getLambda() {
        return lambda;
    }

    public SimGenerator getGenerator() {
        return generator;
    }

    public void setLicznik(int licznik) {
        this.licznik = licznik;
    }

    public int getLicznik() {
        return licznik;
    }

    public int getOgraniczenie() {
        return ograniczenie;
    }

    public List<Dystrybutor> getdBenzyna() {
        return dBenzyna;
    }

    public void setdBenzyna(List<Dystrybutor> dBenzyna) {
        this.dBenzyna = dBenzyna;
    }

    public List<Dystrybutor> getdLPG() {
        return dLPG;
    }

    public void setdLPG(List<Dystrybutor> dLPG) {
        this.dLPG = dLPG;
    }

    public List<Dystrybutor> getdON() {
        return dON;
    }

    public void setdON(List<Dystrybutor> dON) {
        this.dON = dON;
    }

    @Override
    public void reflect(IPublisher iPublisher, INotificationEvent iNotificationEvent) {

    }

    @Override
    public boolean filter(IPublisher iPublisher, INotificationEvent iNotificationEvent) {
        return false;
    }
}
