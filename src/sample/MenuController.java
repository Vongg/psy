package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class MenuController implements Initializable {
    @FXML
    Slider numberOfBenz;
    @FXML
    Slider numberOfLpg;
    @FXML
    Slider numberOfOn;
    @FXML
    Slider queueSize;
    @FXML
    Button startButton;


    Thread t1;

    public void start(ActionEvent event){
        t1.start();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        int numberOfB = (int) numberOfBenz.getValue();
        int numberOfL = (int) numberOfLpg.getValue();
        int numberOfO = (int) numberOfOn.getValue();
        int qSize = (int) queueSize.getValue();

        startButton.setOnAction(event -> start(event));

        t1 = new Thread(new App(numberOfB, numberOfL, numberOfO, qSize));
    }
}
